import Elysia from "elysia";

import { BooksDatabase } from "../db";
import { Book } from "./data/Book";

const bookAPI = new Elysia()
  .decorate("db", new BooksDatabase())
  .get("/books", ({ db }) => db.getBooks())
  .post("/books", async ({ db, body }) => {
    console.log(body);
    const id = (await db.addBook(body as Book)).id;
    console.log(id);
    return { success: true, id };
  })
  .put(
    "/books/:id",
    ({ db, params, body }) => {
      try {
        db.updateBook(parseInt(params.id), body as Book);
        return { success: true };
      } catch (e) {
        return { success: false };
      }
    },
  )
  .delete("/books/:id", ({ db, params }) => {
    try {
      db.deleteBook(parseInt(params.id));
      return { success: true };
    } catch (e) {
      return { success: false };
    }
  });

export { bookAPI };
