import { Elysia } from "elysia";
import { staticPlugin } from "@elysiajs/static";
import { html } from "@elysiajs/html";
import cors from "@elysiajs/cors";

import { pagesRoute } from "./routes/pages";
import { bookAPI } from "./book/api";

const app = new Elysia()
  .use(cors())
  .use(staticPlugin())
  .use(html())
  .use(pagesRoute)
  .get("/script.js", () => Bun.file("src/script.js").text())
  .use(bookAPI);

export { app };
