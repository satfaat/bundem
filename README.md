## npm packages

- https://www.npmjs.com/search?q=svelte

## Bun

```shell
# setup bun
bun init

bun add elysia svelte
bun add bun-types vite -d
#bun add @vitejs/plugin-react -d
bun add @vitejs/plugin-svelte -d
bun add elysia-vite

#bun install elysia svelte
```

```json
//package.json
"scripts": {
    "dev": "bun run --watch index.ts" // enables hot reload
  },
```

# Elysia with Bun runtime

## Getting Started

To get started with this template, simply paste this command into your terminal:

```bash
bun create elysia ./elysia-example
```

## Development

To start the development server run:

```bash
bun run dev
```

Open http://localhost:3000/ with your browser to see the result.

## Svelte

```shell
#bunx create-svelte
bun add svelte
```

## React

```shell
bun add --dev @types/react
bun add -d @types/react
```
